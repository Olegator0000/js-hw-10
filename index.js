const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

tabs.forEach((tab) => {
    tab.addEventListener('click', (e) => {
        const selectedTab = e.target;
        const tabId = selectedTab.id;

        tabContents.forEach((content) => {
            if (content.getAttribute('data-item') === tabId) {
                content.classList.add('tabs-title-imet');
                content.classList.remove('tabs-title-none');
            } else {
                content.classList.add('tabs-title-none');
                content.classList.remove('tabs-title-imet');
            }
        });

        tabs.forEach((tab) => {
            if (tab.id === tabId) {
                tab.classList.add('active');
            } else {
                tab.classList.remove('active');
            }
        });
    });
});


